var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function () {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function (oBici, cb) {
    this.create(oBici, cb);
};

bicicletaSchema.statics.findByCode = function (ncode, cb) {
    return this.findOne({ code: ncode }, cb);
};

bicicletaSchema.statics.removeByCode = function (ncode, cb) {
    return this.deleteOne({ code: ncode }, cb);
};
bicicletaSchema.createInstance=(1, "azul", "tardecita", [-33.33, -33.3355]);

module.exports = mongoose.model('Bicicleta', bicicletaSchema);
/*
var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo,
    this.ubicacion = ubicacion;

}


Bicicleta.allBicis = [];

Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciID) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciID);
    if (aBici) {
        return aBici
    } else {
        throw new Error(`No existe biciccleta con ID: ${aBiciID}`);
    }
}

Bicicleta.removeById = function (aBiciID) {
    //var aBici = Bicicleta.allBicis.findById(aBiciID)
    for (var i = 0; i < Bicicleta.allBicis.length; ++i) {
        if (Bicicleta.allBicis[i].id == aBiciID) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
    }




var b = new Bicicleta(2, "azul", "cross", [-33.3333, -60.999]);
var c = new Bicicleta(3, "violeta", "monta�a", [-33.555, -61.000]);
var d = new Bicicleta(4, "gris", "campo", [-31.3333, -60.950]);


Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);

module.exports = Bicicleta;*/